<?php

namespace Top10\OAuthBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;
use Top10\OAuthBundle\Provider\AbstractProvider;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class UserDataReceivedEvent extends Event
{
    protected $response = null;
    protected $data = null;
    protected $email = null;
    protected $provider = null;

    public function __construct(array $data, $email, AbstractProvider $provider)
    {
        $this->data = $data;
        $this->email = $email;
        $this->provider = $provider;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return AbstractProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

} 