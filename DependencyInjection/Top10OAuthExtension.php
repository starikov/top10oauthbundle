<?php

namespace Top10\OAuthBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class Top10OAuthExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $enabledProviders = array();
        foreach($config as $name => &$params) {
            if(count($params) > 1) {
                $params['enabled'] = true;
                $enabledProviders[] = $name;
            }

            $container->setParameter('top10_oauth.providers.'.$name, $params);
        }

        $container->setParameter('top10_oauth.enabled_providers', $enabledProviders);
    }

    public function getAlias()
    {
        return 'top10_o_auth';
    }
}
