<?php

namespace Top10\OAuthBundle\Provider;

use Symfony\Component\HttpFoundation\Request;
use Top10\OAuthBundle\Exception\OAuthException;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
abstract class AbstractProvider
{
    /**
     * @var array
     */
    protected $params = array();

    /**
     * @var \Buzz\Client\Curl
     */
    protected $buzz;

    /**
     * @var boolean
     */
    protected $debug;

    /**
     * @param array $params
     * @param bool $debug
     */
    public function __construct(array $params, $debug = false)
    {
        $this->buzz = new \Buzz\Client\Curl();
        $this->debug = $debug;

        $this->params = array_merge($this->getParams(), $params);
    }

    /**
     * @return array
     */
    abstract protected function getParams();

    /**
     * @param string $redirectUri
     * @param string $state
     * @param string $scope
     * @return string
     */
    public function generateAuthorizeUri($redirectUri, $state, $scope = null)
    {
        $params = array(
            'client_id' => $this->getAppId(),
            'redirect_uri' => $redirectUri,
            'state' => $state,
            'response_type' => 'code',
            'scope' => implode(',', $this->getAppScope()),
        );

        if($scope) {
            $params['scope'] = $scope;
        }

        $authorizeUri = $this->getAuthorizationUrl() . '?' . http_build_query($params);

        return $authorizeUri;
    }

    /**
     * @param $redirectUri
     * @param $code
     * @return \Buzz\Message\Request
     */
    public function generateTokenRequest($redirectUri, $code)
    {
        $params = array(
            'client_id' => $this->getAppId(),
            'redirect_uri' => $redirectUri,
            'client_secret' => $this->getSecret(),
            'code' => $code,
        );

        $tokenUri = $this->getAccessTokenUrl() . '?' . http_build_query($params);

        $http_request = new \Buzz\Message\Request('GET', '', $tokenUri);

        return $http_request;
    }

    /**
     * @return string
     */
    public function getAppId()
    {
        return $this->params['app_id'];
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl()
    {
        return $this->params['authorization_url'];
    }

    /**
     * @return string
     */
    public function getAccessTokenUrl()
    {
        return $this->params['access_token_url'];
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->params['secret'];
    }

    /**
     * @return string
     */
    public function getInfosUrl()
    {
        return $this->params['infos_url'];
    }

    /**
     * @param \Buzz\Message\Request $http_request
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return array
     */
    public function requestAccessToken(\Buzz\Message\Request $http_request)
    {
        $http_response = $this->request($http_request);
        $credentials = $this->parseAccessTokenRequest($http_response);

        return $credentials;
    }

    /**
     * @param \Buzz\Message\Request $http_request
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return \Buzz\Message\Response
     */
    public function request(\Buzz\Message\Request $http_request)
    {
        $http_response = new \Buzz\Message\Response();

        try {
            $this->buzz->send($http_request, $http_response);
        }
        catch(\RuntimeException $failed) {
            if($this->debug) {
                echo $http_request . PHP_EOL . PHP_EOL;
            }

            throw $this->generateException(
                sprintf('Network error: %s', $failed->getMessage())
            );
        }

        if($http_response->getStatusCode() != 200) {
            if($this->debug) {
                echo $http_request . PHP_EOL . PHP_EOL;
                echo $http_response . PHP_EOL . PHP_EOL;
                var_dump($http_response->getHeaders());
            }

            throw $this->generateException(
                sprintf('Request failed. Response code: %d', $http_response->getStatusCode())
            );
        }

        return $http_response;
    }

    /**
     * @param \Buzz\Message\Request $http_request
     * @param boolean $check
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return array
     */
    public function requestJson(\Buzz\Message\Request $http_request, $check = true)
    {
        $http_response = $this->request($http_request);
        $response = json_decode($http_response->getContent(), true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            throw $this->generateException( sprintf('Invalid json format') );
        }

        if($check) {
            $error = $this->isErrorAtResponse($response);
            if($error !== false) {
                throw $this->generateException($error);
            }
        }

        return $response;
    }

    /**
     * @param \Buzz\Message\Response $http_response
     * @return mixed
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     */
    protected function parseAccessTokenRequest(\Buzz\Message\Response $http_response)
    {
        $data = json_decode($http_response->getContent(), true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            throw $this->generateException(
                sprintf('Ошибка преобразования json. %s', $http_response->getContent())
            );
        }

        return $data;
    }

    /**
     * @param $msg
     * @return \Top10\OAuthBundle\Exception\OAuthException
     */
    public function generateException($msg)
    {
        return new OAuthException($msg);
    }

    /**
     * @param array $credentials
     * @return mixed
     */
    abstract public function getUserData(array $credentials);

    /**
     * @return array
     */
    public function getAppScope()
    {
        return array();
    }

    /**
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return string
     */
    public function getAlias()
    {
        if(false === preg_match('/([^\\\\]+)Provider$/', get_class($this), $matches)) {
            throw $this->generateException('Некашерное имя класса.');
        }

        $name = $matches[1];

        return strtolower($name);
    }

    /**
     * @param array $userData
     * @return string
     */
    public function generateEmailIfNotExist(array $userData)
    {
        if(array_key_exists('email', $userData) && !empty($userData['email'])) {
            return $userData['email'];
        }

        $email = sprintf('%s@%s.com', $userData['uid'], $this->getAlias());

        return $email;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return bool|string
     */
    public function isErrorOnCheckCode(Request $request)
    {
        return false;
    }

    /**
     * @param array $response
     * @return bool|string
     */
    public function isErrorAtResponse(array $response)
    {
        return false;
    }
}