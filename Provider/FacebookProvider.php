<?php

namespace Top10\OAuthBundle\Provider;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class FacebookProvider extends AbstractProvider
{
    /**
     * {@inheritdoc}
     */
    protected function parseAccessTokenRequest(\Buzz\Message\Response $http_response)
    {
        parse_str($http_response->getContent(), $data);

        if(empty($data)) {
            throw $this->generateException(
                sprintf('Ошибка преобразования ответа Facebook. %s', $http_response->getContent())
            );
        }

        return $data;
    }

    /**
     * @param array $credentials
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return array
     */
    public function getUserData(array $credentials)
    {
        $response = $this->getOriginUserData($credentials, 'id,username,name,email');

        // TODO check error
        /*
            {
              "error": {
                "message": "An active access token must be used to query information about the current user.",
                "type": "OAuthException",
                "code": 2500
              }
            }
        */

        // приводим к одному виду
        return array(
            'uid' => $response['id'],
            'name' => $response['name'],
            'email' => isset($response['email']) ? $response['email'] : null,
            'phone' => null,
            'screen_name' => isset($response['username']) ? $response['username'] : null
        );
    }

    /**
     * @param array $credentials
     * @param string $fields
     * @return array
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     */
    public function getOriginUserData(array $credentials, $fields = 'id,email')
    {
        /*
        array (size=3)
            'access_token' => string 'c506cc353b6ad241dc5761d554db5c223d4404ed71752d6800a24d6a1531c205e9728b7bbb920afa6a9eb'
            'expires' => int 86400
        */

        // https://developers.facebook.com/docs/reference/api/user/
        $params = array(
            'access_token' => $credentials['access_token'],
            'fields' => $fields
        );

        $info_url = $this->getInfosUrl() . '?' . http_build_query($params);

        $http_request = new \Buzz\Message\Request('GET', '', $info_url);
        $http_response = new \Buzz\Message\Response();
        $this->buzz->send($http_request, $http_response);

        if ($http_response->getStatusCode() != 200) {
            throw $this->generateException(
                sprintf('Can not get user_data. Code: %d', $http_response->getStatusCode())
            );
        }

        $response = json_decode($http_response->getContent(), true);

        if (json_last_error() !== JSON_ERROR_NONE) {
            throw $this->generateException(
                sprintf('Json transform error. %s', $http_response->getContent())
            );
        }

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function isErrorOnCheckCode(\Symfony\Component\HttpFoundation\Request $request)
    {
        $getParams = $request->query->all();
        $error_code = isset($getParams['error_code']) ? $getParams['error_code'] : null;
        $error_message = isset($getParams['error_message']) ? $getParams['error_message'] : null;

        if($error_code) {
            if($error_message) {
                return $error_message;
            }

            $error_message = sprintf('oauth.unknown_error');

            return $error_message;
        }

        return false;
    }

    /**
     * https://developers.facebook.com/docs/facebook-login/permissions/#adding
     *
     * {@inheritdoc}
     */
    public function getAppScope()
    {
        return array('email');
    }

    /**
     * {@inheritdoc}
     */
    protected function getParams()
    {
        return array(
            'authorization_url' => 'https://www.facebook.com/dialog/oauth',
            'access_token_url' => 'https://graph.facebook.com/oauth/access_token',
            'infos_url' => 'https://graph.facebook.com/me',
            'revoke_token_url' => 'https://graph.facebook.com/me/permissions',
        );
    }
}