<?php

namespace Top10\OAuthBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;
use Top10\OAuthBundle\Provider\AbstractProvider;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class TokenReceivedEvent extends Event
{
    protected $response = null;
    protected $credentials = null;
    protected $provider = null;

    public function __construct(array $credentials, AbstractProvider $provider)
    {
        $this->credentials = $credentials;
        $this->provider = $provider;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getCredentials()
    {
        return $this->credentials;
    }

    /**
     * @return AbstractProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}