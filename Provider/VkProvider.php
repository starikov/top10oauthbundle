<?php

namespace Top10\OAuthBundle\Provider;

/**
 * TODO: новая версия API 5.2 http://vk.com/dev/auth_sites
 *
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class VkProvider extends AbstractProvider
{
    /**
     * @param array $credentials
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return array
     */
    public function getUserData(array $credentials)
    {
        /*
        array (size=3)
            'access_token' => string 'c506cc353b6ad241dc5761d554db5c223d4404ed71752d6800a24d6a1531c205e9728b7bbb920afa6a9eb'
            'expires_in' => int 86400
            'user_id' => int 2096592
        */

        // http://vk.com/developers.php?oid=-1&p=users.get
        $params = array(
            'access_token' => $credentials['access_token'],
            // недокументированная возможность
            // но если не передавать uid, то вернется текущий пользователь :)
            //'uids' => $credentials['user_id'],
            'fields' => 'first_name,last_name,contacts,screen_name'
        );

        $info_url = $this->getInfosUrl() . '?' . http_build_query($params);

        $http_request = new \Buzz\Message\Request('GET', '', $info_url);
        $response = $this->requestJson($http_request, true);

        // http://vk.com/pages?oid=-1&p=%D0%9E%D0%BF%D0%B8%D1%81%D0%B0%D0%BD%D0%B8%D0%B5_%D0%BF%D0%BE%D0%BB%D0%B5%D0%B9_%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D0%B0_fields

        $user_data = $response['response'][0];

        $phone = isset($user_data['mobile_phone']) ? $user_data['mobile_phone'] : null;
        $phone = ($phone === null && isset($user_data['home_phone'])) ? $user_data['home_phone'] : $phone;

        // приводим к одному виду
        return array(
            'uid' => $user_data['uid'],
            'name' => $user_data['last_name'] . ' ' . $user_data['first_name'],
            'email' => null,
            'phone' => $phone,
            'screen_name' => $user_data['screen_name']
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isErrorOnCheckCode(\Symfony\Component\HttpFoundation\Request $request)
    {
        $getParams = $request->query->all();

        $error = isset($getParams['error']) ? $getParams['error'] : null;
        $error_reason = isset($getParams['error_reason']) ? $getParams['error_reason'] : null;
        $error_description = isset($getParams['error_description']) ? $getParams['error_description'] : null;

        if($error || $error_description || $error_reason) {
            if($error_description) {
                return $error_description;
            }

            if($error_reason) {
                return $error_reason;
            }

            $error_message = sprintf('oauth.unknown_error');

            return $error_message;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isErrorAtResponse(array $response)
    {
        if(isset($response['error'])) {
            $error = $response['error'];
            $error_code = isset($error['error_code']) ? $error['error_code'] : null;
            $error_msg = isset($error['error_msg']) ? $error['error_msg'] : null;

            if($error_msg) {
                $error_message = sprintf('%s(%d)', $error_msg, $error_code);
            }
            else {
                $error_message = sprintf('oauth.unknown_error(%d)', $error_code);
            }

            return $error_message;
        }

        if(!isset($response['response'])) {
            $error_message = sprintf('oauth.empty_response');

            return $error_message;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAppScope()
    {
        return array();
    }

    /**
     * {@inheritdoc}
     */
    protected function getParams()
    {
        return array(
            'authorization_url' => 'https://oauth.vk.com/authorize',
            'access_token_url'  => 'https://oauth.vk.com/access_token',
            'infos_url'         => 'https://api.vk.com/method/users.get',
        );
    }
}