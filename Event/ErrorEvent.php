<?php

namespace Top10\OAuthBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class ErrorEvent extends Event
{
    protected $response = null;
    protected $error = null;

    public function __construct($error)
    {
        $this->response = $response;
        $this->error = $error;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse(Response $response)
    {
        $this->response = $response;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }
} 