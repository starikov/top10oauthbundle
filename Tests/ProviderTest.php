<?php

namespace Top10\OAuthBundle\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class ProviderTest extends WebTestCase
{
    protected function setUp()
    {
        parent::setUp();
    }

    public function testProviderUtils()
    {
        $client = self::createClient();

        $prov = $client->getContainer()->get('oauth.provider.vk');
        $this->assertEquals('vk', $prov->getAlias());
    }
}
