<?php

namespace Top10\OAuthBundle\Provider;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
class GplusProvider extends AbstractProvider
{
    /**
     * {@inheritdoc}
     */
    public function generateTokenRequest($redirectUri, $code)
    {
        $params = array(
            'client_id' => $this->getAppId(),
            'redirect_uri' => $redirectUri,
            'client_secret' => $this->getSecret(),
            'code' => $code,
            'grant_type' => 'authorization_code'
        );

        $content = http_build_query($params);

        $url = parse_url($this->getAccessTokenUrl());

        $http_request = new \Buzz\Message\Request('POST', $url['path'], $url['scheme'] . '://' .$url['host']);
        $http_request->setProtocolVersion(1.1);
        $http_request->setContent($content);
        $http_request->setHeaders(array(
            'Content-Type' => 'application/x-www-form-urlencoded',
            'Content-Length' => strlen($content),
        ));

        return $http_request;
    }

    /**
     * @param array $credentials
     * @throws \Top10\OAuthBundle\Exception\OAuthException
     * @return array
     */
    public function getUserData(array $credentials)
    {
        /*
            access_token 	A token that can be sent to a Google API
            id_token 	    A JWT that contains identity information about the user that is digitally signed by Google
            expires_in 	    The remaining lifetime on the Access Token
            token_type 	    Indicates the type of token returned. At this time, this field will always have the value Bearer
        */

        // https://developers.google.com/accounts/docs/OAuth2Login?hl=ru
        $params = array(
            'access_token' => $credentials['access_token']
        );

        $info_url = $this->getInfosUrl() . '?' . http_build_query($params);

        $http_request = new \Buzz\Message\Request('GET', '', $info_url);
        $http_response = new \Buzz\Message\Response();
        $this->buzz->send($http_request, $http_response);

        if($http_response->getStatusCode() != 200) {
            throw $this->generateException(
                sprintf('Не могу получить user_data. Код ответа: %d', $http_response->getStatusCode())
            );
        }

        $response = json_decode($http_response->getContent(), true);

        if(json_last_error() !== JSON_ERROR_NONE) {
            throw $this->generateException(
                sprintf('Ошибка преобразования json')
            );
        }

        // https://developers.google.com/accounts/docs/OAuth2Login?hl=ru#obtaininguserprofileinformation

        // TODO check
        return array(
            'uid' => $response['sub'],
            'name' => $response['name'],
            'email' => isset($response['email']) ? $response['email'] : null,
            'phone' => null,
            'screen_name' => null
        );
    }

    /**
     * {@inheritdoc}
     */
    public function isErrorOnCheckCode(\Symfony\Component\HttpFoundation\Request $request)
    {
        $getParams = $request->query->all();
        $error = isset($getParams['error']) ? $getParams['error'] : null;

        if($error) {
            return $error;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function getAppScope()
    {
        return array('openid email profile');
    }

    /**
     * {@inheritdoc}
     */
    protected function getParams()
    {
        return array(
            'authorization_url' => 'https://accounts.google.com/o/oauth2/auth',
            'access_token_url' => 'https://accounts.google.com/o/oauth2/token',
            'infos_url' => 'https://www.googleapis.com/oauth2/v3/userinfo',
            'revoke_token_url' => 'https://accounts.google.com/o/oauth2/revoke',
        );
    }
}