<?php

namespace Top10\OAuthBundle\Event;

/**
 * @author: Alexey Belousov <forgottenbas@gmail.com>
 */
final class OAuthEvents
{
    /**
     * The <top10_oauth.on_token_received>
     *
     * The event listener receives an
     * Top10\OAuthBundle\Event\TokenReceivedEvent object
     *
     * @var string
     */
    const TOKEN_RECEIVED = 'top10_oauth.on_token_received';

    /**
     * The <top10_oauth.on_user_data_received>
     *
     * The event listener receives an
     * Top10\OAuthBundle\Event\UserDataReceivedEvent object
     *
     * @var string
     */
    const USER_DATA_RECEIVED = 'top10_oauth.on_user_data_received';

    /**
     * The <top10_oauth.on_error>
     *
     * The event listener receives an
     * Top10\OAuthBundle\Event\ErrorEvent object
     *
     * @var string
     */
    const ERROR = 'top10_oauth.on_error';
}