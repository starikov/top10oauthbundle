<?php

namespace Top10\OAuthBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Top10\OAuthBundle\Event;
use Top10\OAuthBundle\Provider\AbstractProvider;

class OAuthController extends Controller
{
    /**
     * @Route("/connect/{provider}", name="oauth_connect")
     */
    public function connectAction($provider)
    {
        if(!in_array($provider, $this->container->getParameter('top10_oauth.enabled_providers'))) {
            throw $this->createNotFoundException();
        }

        $request = $this->getRequest();
        $state = md5(uniqid(rand(), true));
        $request->getSession()->set('state', $state);

        /** @var $oauthProvider AbstractProvider */
        $oauthProvider = $this->container->get(sprintf('oauth.provider.%s', $provider));

        $authorizeUri = $oauthProvider->generateAuthorizeUri(
            $this->generateUrl('oauth_check_token', array('provider' => $provider), true),
            $state
        );

        return $this->redirect($authorizeUri);
    }

    /**
     * @Route("/check/{provider}", name="oauth_check_token")
     */
    public function checkAction($provider)
    {
        if(!in_array($provider, $this->container->getParameter('top10_oauth.enabled_providers'))) {
            throw $this->createNotFoundException();
        }

        /** @var $oauthProvider AbstractProvider */
        $oauthProvider = $this->container->get(sprintf('oauth.provider.%s', $provider));
        $evd = $this->get('event_dispatcher');

        # get token
        $request = $this->getRequest();

        $code = $request->query->get('code', false);
        $state = $request->query->get('state', false);
        $request->getSession()->remove('state');

        // @TODO: check state

        if(!$code) {
            throw new \RuntimeException('Sorry dude - something is totally broken');
        }

        $error = $oauthProvider->isErrorOnCheckCode($request);
        if($error) {
            $event = new Event\ErrorEvent($error);
            $evd->dispatch(Event\OAuthEvents::ERROR, $event);

            $response = $event->getResponse();
            if(!$response instanceof Response) {
                throw new \RuntimeException('Must be an event listener that will create the desired response object');
            }

            return $response;
        }

        $tokenRequest = $oauthProvider->generateTokenRequest(
            $this->generateUrl('oauth_check_token', array('provider' => $provider), true),
            $code
        );

        // request access token
        $credentials = $oauthProvider->requestAccessToken($tokenRequest);
        $event = new Event\TokenReceivedEvent($credentials, $oauthProvider);
        $evd->dispatch(Event\OAuthEvents::TOKEN_RECEIVED, $event);

        $response = $event->getResponse();
        if($response instanceof Response) {
            return $response;
        }

        // request user data
        $userData = $oauthProvider->getUserData($credentials);
        $email = $oauthProvider->generateEmailIfNotExist($userData);

        $event = new Event\UserDataReceivedEvent($userData, $email, $oauthProvider);
        $evd->dispatch(Event\OAuthEvents::USER_DATA_RECEIVED, $event);

        $response = $event->getResponse();
        if(!$response instanceof Response) {
            throw new \RuntimeException('Must be an event listener that will create the desired response object');
        }

        return $response;
    }
}
