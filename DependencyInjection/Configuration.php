<?php

namespace Top10\OAuthBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('top10_o_auth');

        $this->addFacebookSection($rootNode);
        $this->addVkSection($rootNode);
        $this->addTwitterSection($rootNode);
        $this->addGplusSection($rootNode);

        return $treeBuilder;
    }

    protected function addFacebookSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('facebook')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('app_id')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('secret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('authorization_url')->end()
                        ->scalarNode('access_token_url')->end()
                        ->scalarNode('revoke_token_url')->end()
                        ->scalarNode('infos_url')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    protected function addVkSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('vk')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('app_id')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('secret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('authorization_url')->end()
                        ->scalarNode('access_token_url')->end()
                        ->scalarNode('infos_url')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    protected function addTwitterSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('twitter')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('app_id')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('secret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('authorization_url')->end()
                        ->scalarNode('access_token_url')->end()
                        ->scalarNode('request_token_url')->end()
                        ->scalarNode('infos_url')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    protected function addGplusSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('gplus')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->booleanNode('enabled')->defaultFalse()->end()
                        ->scalarNode('app_id')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('secret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('authorization_url')->end()
                        ->scalarNode('access_token_url')->end()
                        ->scalarNode('revoke_token_url')->end()
                        ->scalarNode('infos_url')->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}